
package net.mcreator.starwarsarmors.item;

import net.minecraftforge.registries.ObjectHolder;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.World;
import net.minecraft.util.ResourceLocation;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ArmorItem;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.BipedModel;

import net.mcreator.starwarsarmors.procedures.AllarmorProcedure;
import net.mcreator.starwarsarmors.StarWarsArmorsModElements;

import java.util.Map;
import java.util.HashMap;

import com.mojang.blaze3d.vertex.IVertexBuilder;
import com.mojang.blaze3d.matrix.MatrixStack;

@StarWarsArmorsModElements.ModElement.Tag
public class B1Item extends StarWarsArmorsModElements.ModElement {
	@ObjectHolder("star_wars_armors:b_1_helmet")
	public static final Item helmet = null;
	@ObjectHolder("star_wars_armors:b_1_chestplate")
	public static final Item body = null;
	@ObjectHolder("star_wars_armors:b_1_leggings")
	public static final Item legs = null;
	@ObjectHolder("star_wars_armors:b_1_boots")
	public static final Item boots = null;
	public B1Item(StarWarsArmorsModElements instance) {
		super(instance, 2);
	}

	@Override
	public void initElements() {
		IArmorMaterial armormaterial = new IArmorMaterial() {
			@Override
			public int getDurability(EquipmentSlotType slot) {
				return new int[]{13, 15, 16, 11}[slot.getIndex()] * 1024;
			}

			@Override
			public int getDamageReductionAmount(EquipmentSlotType slot) {
				return new int[]{0, 0, 0, 0}[slot.getIndex()];
			}

			@Override
			public int getEnchantability() {
				return 0;
			}

			@Override
			public net.minecraft.util.SoundEvent getSoundEvent() {
				return (net.minecraft.util.SoundEvent) ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(""));
			}

			@Override
			public Ingredient getRepairMaterial() {
				return Ingredient.EMPTY;
			}

			@OnlyIn(Dist.CLIENT)
			@Override
			public String getName() {
				return "b_1";
			}

			@Override
			public float getToughness() {
				return 0f;
			}

			@Override
			public float getKnockbackResistance() {
				return 0f;
			}
		};
		elements.items.add(() -> new ArmorItem(armormaterial, EquipmentSlotType.HEAD, new Item.Properties().group(ItemGroup.COMBAT)) {
			@Override
			@OnlyIn(Dist.CLIENT)
			public BipedModel getArmorModel(LivingEntity living, ItemStack stack, EquipmentSlotType slot, BipedModel defaultModel) {
				BipedModel armorModel = new BipedModel(1);
				armorModel.bipedHead = new ModelHeadB1().Head;
				armorModel.isSneak = living.isSneaking();
				armorModel.isSitting = defaultModel.isSitting;
				armorModel.isChild = living.isChild();
				return armorModel;
			}

			@Override
			public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type) {
				return "star_wars_armors:textures/headb1.png";
			}

			@Override
			public void onArmorTick(ItemStack itemstack, World world, PlayerEntity entity) {
				super.onArmorTick(itemstack, world, entity);
				double x = entity.getPosX();
				double y = entity.getPosY();
				double z = entity.getPosZ();
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("entity", entity);
					AllarmorProcedure.executeProcedure($_dependencies);
				}
			}
		}.setRegistryName("b_1_helmet"));
		elements.items.add(() -> new ArmorItem(armormaterial, EquipmentSlotType.CHEST, new Item.Properties().group(ItemGroup.COMBAT)) {
			@Override
			@OnlyIn(Dist.CLIENT)
			public BipedModel getArmorModel(LivingEntity living, ItemStack stack, EquipmentSlotType slot, BipedModel defaultModel) {
				BipedModel armorModel = new BipedModel(1);
				armorModel.bipedBody = new ModelBodyB1().Body;
				armorModel.bipedLeftArm = new ModelBodyB1().Armleft;
				armorModel.bipedRightArm = new ModelBodyB1().Armright;
				armorModel.isSneak = living.isSneaking();
				armorModel.isSitting = defaultModel.isSitting;
				armorModel.isChild = living.isChild();
				return armorModel;
			}

			@Override
			public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type) {
				return "star_wars_armors:textures/bodyb1.png";
			}

			@Override
			public void onArmorTick(ItemStack itemstack, World world, PlayerEntity entity) {
				double x = entity.getPosX();
				double y = entity.getPosY();
				double z = entity.getPosZ();
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("entity", entity);
					AllarmorProcedure.executeProcedure($_dependencies);
				}
			}
		}.setRegistryName("b_1_chestplate"));
		elements.items.add(() -> new ArmorItem(armormaterial, EquipmentSlotType.LEGS, new Item.Properties().group(ItemGroup.COMBAT)) {
			@Override
			@OnlyIn(Dist.CLIENT)
			public BipedModel getArmorModel(LivingEntity living, ItemStack stack, EquipmentSlotType slot, BipedModel defaultModel) {
				BipedModel armorModel = new BipedModel(1);
				armorModel.bipedLeftLeg = new ModelLegsB1().legleft;
				armorModel.bipedRightLeg = new ModelLegsB1().legright;
				armorModel.isSneak = living.isSneaking();
				armorModel.isSitting = defaultModel.isSitting;
				armorModel.isChild = living.isChild();
				return armorModel;
			}

			@Override
			public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type) {
				return "star_wars_armors:textures/legsb1.png";
			}

			@Override
			public void onArmorTick(ItemStack itemstack, World world, PlayerEntity entity) {
				double x = entity.getPosX();
				double y = entity.getPosY();
				double z = entity.getPosZ();
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("entity", entity);
					AllarmorProcedure.executeProcedure($_dependencies);
				}
			}
		}.setRegistryName("b_1_leggings"));
		elements.items.add(() -> new ArmorItem(armormaterial, EquipmentSlotType.FEET, new Item.Properties().group(ItemGroup.COMBAT)) {
			@Override
			@OnlyIn(Dist.CLIENT)
			public BipedModel getArmorModel(LivingEntity living, ItemStack stack, EquipmentSlotType slot, BipedModel defaultModel) {
				BipedModel armorModel = new BipedModel(1);
				armorModel.bipedLeftLeg = new ModelFeetB1().footleft;
				armorModel.bipedRightLeg = new ModelFeetB1().footright;
				armorModel.isSneak = living.isSneaking();
				armorModel.isSitting = defaultModel.isSitting;
				armorModel.isChild = living.isChild();
				return armorModel;
			}

			@Override
			public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type) {
				return "star_wars_armors:textures/feetb1.png";
			}

			@Override
			public void onArmorTick(ItemStack itemstack, World world, PlayerEntity entity) {
				double x = entity.getPosX();
				double y = entity.getPosY();
				double z = entity.getPosZ();
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("entity", entity);
					AllarmorProcedure.executeProcedure($_dependencies);
				}
			}
		}.setRegistryName("b_1_boots"));
	}
	// Made with Blockbench 3.9.3
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelFeetB1 extends EntityModel<Entity> {
		private final ModelRenderer footright;
		private final ModelRenderer footleft;
		public ModelFeetB1() {
			textureWidth = 32;
			textureHeight = 32;
			footright = new ModelRenderer(this);
			footright.setRotationPoint(-2.0F, 12.0F, 0.0F);
			footright.setTextureOffset(11, 6).addBox(0.0F, 10.0F, -2.0F, 1.0F, 1.0F, 3.0F, 0.0F, false);
			footright.setTextureOffset(0, 6).addBox(-1.0F, 11.0F, -4.0F, 3.0F, 1.0F, 5.0F, 0.0F, false);
			footleft = new ModelRenderer(this);
			footleft.setRotationPoint(2.0F, 12.0F, 0.0F);
			footleft.setTextureOffset(11, 0).addBox(-1.0F, 10.0F, -2.0F, 1.0F, 1.0F, 3.0F, 0.0F, false);
			footleft.setTextureOffset(0, 0).addBox(-2.0F, 11.0F, -4.0F, 3.0F, 1.0F, 5.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			footright.render(matrixStack, buffer, packedLight, packedOverlay);
			footleft.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
		}
	}

	// Made with Blockbench 3.9.3
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelHeadB1 extends EntityModel<Entity> {
		private final ModelRenderer Head;
		private final ModelRenderer cube_r1;
		public ModelHeadB1() {
			textureWidth = 32;
			textureHeight = 32;
			Head = new ModelRenderer(this);
			Head.setRotationPoint(0.5F, 0.0F, 0.0F);
			Head.setTextureOffset(0, 10).addBox(-1.0F, -6.0F, 0.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);
			Head.setTextureOffset(4, 10).addBox(0.0F, -7.0F, 1.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
			Head.setTextureOffset(0, 17).addBox(-2.0F, -7.0F, 1.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
			cube_r1 = new ModelRenderer(this);
			cube_r1.setRotationPoint(-0.5F, 0.0F, 0.0F);
			Head.addChild(cube_r1);
			setRotationAngle(cube_r1, -0.5672F, 0.0F, 0.0F);
			cube_r1.setTextureOffset(0, 0).addBox(-1.5F, -7.0F, -4.5F, 3.0F, 8.0F, 2.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Head.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
		}
	}

	// Made with Blockbench 3.9.3
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelBodyB1 extends EntityModel<Entity> {
		private final ModelRenderer Body;
		private final ModelRenderer Armleft;
		private final ModelRenderer Armright;
		public ModelBodyB1() {
			textureWidth = 64;
			textureHeight = 64;
			Body = new ModelRenderer(this);
			Body.setRotationPoint(0.0F, 0.0F, 0.0F);
			Body.setTextureOffset(8, 25).addBox(-1.0F, 7.0F, -2.0F, 2.0F, 6.0F, 4.0F, 0.0F, false);
			Body.setTextureOffset(0, 0).addBox(-4.0F, 1.0F, -2.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);
			Body.setTextureOffset(8, 39).addBox(-3.0F, 7.0F, -1.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);
			Body.setTextureOffset(12, 41).addBox(-2.0F, 9.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Body.setTextureOffset(12, 39).addBox(1.0F, 9.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
			Body.setTextureOffset(4, 39).addBox(2.0F, 7.0F, -1.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);
			Body.setTextureOffset(24, 0).addBox(-4.0F, -1.0F, 2.0F, 8.0F, 8.0F, 3.0F, 0.0F, false);
			Body.setTextureOffset(0, 11).addBox(-4.0F, 0.0F, -1.0F, 8.0F, 1.0F, 3.0F, 0.0F, false);
			Body.setTextureOffset(0, 39).addBox(3.0F, -5.0F, 4.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);
			Armleft = new ModelRenderer(this);
			Armleft.setRotationPoint(5.0F, 2.0F, 0.0F);
			Armleft.setTextureOffset(22, 11).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);
			Armright = new ModelRenderer(this);
			Armright.setRotationPoint(-5.0F, 2.0F, 0.0F);
			Armright.setTextureOffset(0, 25).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			Body.render(matrixStack, buffer, packedLight, packedOverlay);
			Armleft.render(matrixStack, buffer, packedLight, packedOverlay);
			Armright.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
		}
	}

	// Made with Blockbench 3.9.3
	// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
	// Paste this class into your mod and generate all required imports
	public static class ModelLegsB1 extends EntityModel<Entity> {
		private final ModelRenderer legright;
		private final ModelRenderer legleft;
		public ModelLegsB1() {
			textureWidth = 32;
			textureHeight = 32;
			legright = new ModelRenderer(this);
			legright.setRotationPoint(-2.0F, 12.0F, 0.0F);
			legright.setTextureOffset(0, 12).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			legright.setTextureOffset(8, 12).addBox(0.0F, 1.0F, -1.0F, 1.0F, 3.0F, 2.0F, 0.0F, false);
			legright.setTextureOffset(6, 8).addBox(-0.5F, 4.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			legright.setTextureOffset(0, 6).addBox(0.0F, 6.0F, -1.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);
			legleft = new ModelRenderer(this);
			legleft.setRotationPoint(2.0F, 12.0F, 0.0F);
			legleft.setTextureOffset(6, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			legleft.setTextureOffset(12, 2).addBox(-1.0F, 1.0F, -1.0F, 1.0F, 3.0F, 2.0F, 0.0F, false);
			legleft.setTextureOffset(4, 4).addBox(-1.5F, 4.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
			legleft.setTextureOffset(0, 0).addBox(-1.0F, 6.0F, -1.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);
		}

		@Override
		public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue,
				float alpha) {
			legright.render(matrixStack, buffer, packedLight, packedOverlay);
			legleft.render(matrixStack, buffer, packedLight, packedOverlay);
		}

		public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
			modelRenderer.rotateAngleX = x;
			modelRenderer.rotateAngleY = y;
			modelRenderer.rotateAngleZ = z;
		}

		public void setRotationAngles(Entity e, float f, float f1, float f2, float f3, float f4) {
		}
	}
}
