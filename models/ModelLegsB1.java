// Made with Blockbench 3.9.3
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class ModelLegsB1 extends EntityModel<Entity> {
	private final ModelRenderer legright;
	private final ModelRenderer legleft;

	public ModelLegsB1() {
		textureWidth = 32;
		textureHeight = 32;

		legright = new ModelRenderer(this);
		legright.setRotationPoint(-2.0F, 12.0F, 0.0F);
		legright.setTextureOffset(0, 12).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		legright.setTextureOffset(8, 12).addBox(0.0F, 1.0F, -1.0F, 1.0F, 3.0F, 2.0F, 0.0F, false);
		legright.setTextureOffset(6, 8).addBox(-0.5F, 4.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		legright.setTextureOffset(0, 6).addBox(0.0F, 6.0F, -1.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);

		legleft = new ModelRenderer(this);
		legleft.setRotationPoint(2.0F, 12.0F, 0.0F);
		legleft.setTextureOffset(6, 0).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		legleft.setTextureOffset(12, 2).addBox(-1.0F, 1.0F, -1.0F, 1.0F, 3.0F, 2.0F, 0.0F, false);
		legleft.setTextureOffset(4, 4).addBox(-1.5F, 4.0F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		legleft.setTextureOffset(0, 0).addBox(-1.0F, 6.0F, -1.0F, 1.0F, 4.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		legright.render(matrixStack, buffer, packedLight, packedOverlay);
		legleft.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
	}
}