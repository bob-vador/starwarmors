// Made with Blockbench 3.9.3
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class ModelHeadB1 extends EntityModel<Entity> {
	private final ModelRenderer Head;
	private final ModelRenderer cube_r1;

	public ModelHeadB1() {
		textureWidth = 32;
		textureHeight = 32;

		Head = new ModelRenderer(this);
		Head.setRotationPoint(0.5F, 0.0F, 0.0F);
		Head.setTextureOffset(0, 10).addBox(-1.0F, -6.0F, 0.0F, 1.0F, 6.0F, 1.0F, 0.0F, false);
		Head.setTextureOffset(4, 10).addBox(0.0F, -7.0F, 1.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		Head.setTextureOffset(0, 17).addBox(-2.0F, -7.0F, 1.0F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(-0.5F, 0.0F, 0.0F);
		Head.addChild(cube_r1);
		setRotationAngle(cube_r1, -0.5672F, 0.0F, 0.0F);
		cube_r1.setTextureOffset(0, 0).addBox(-1.5F, -7.0F, -4.5F, 3.0F, 8.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Head.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
	}
}