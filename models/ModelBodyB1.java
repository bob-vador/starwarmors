// Made with Blockbench 3.9.3
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class ModelBodyB1 extends EntityModel<Entity> {
	private final ModelRenderer Body;
	private final ModelRenderer Armleft;
	private final ModelRenderer Armright;

	public ModelBodyB1() {
		textureWidth = 64;
		textureHeight = 64;

		Body = new ModelRenderer(this);
		Body.setRotationPoint(0.0F, 0.0F, 0.0F);
		Body.setTextureOffset(8, 25).addBox(-1.0F, 7.0F, -2.0F, 2.0F, 6.0F, 4.0F, 0.0F, false);
		Body.setTextureOffset(0, 0).addBox(-4.0F, 1.0F, -2.0F, 8.0F, 6.0F, 4.0F, 0.0F, false);
		Body.setTextureOffset(8, 39).addBox(-3.0F, 7.0F, -1.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		Body.setTextureOffset(12, 41).addBox(-2.0F, 9.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		Body.setTextureOffset(12, 39).addBox(1.0F, 9.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		Body.setTextureOffset(4, 39).addBox(2.0F, 7.0F, -1.0F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		Body.setTextureOffset(24, 0).addBox(-4.0F, -1.0F, 2.0F, 8.0F, 8.0F, 3.0F, 0.0F, false);
		Body.setTextureOffset(0, 11).addBox(-4.0F, 0.0F, -1.0F, 8.0F, 1.0F, 3.0F, 0.0F, false);
		Body.setTextureOffset(0, 39).addBox(3.0F, -5.0F, 4.0F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		Armleft = new ModelRenderer(this);
		Armleft.setRotationPoint(5.0F, 2.0F, 0.0F);
		Armleft.setTextureOffset(22, 11).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);

		Armright = new ModelRenderer(this);
		Armright.setRotationPoint(-5.0F, 2.0F, 0.0F);
		Armright.setTextureOffset(0, 25).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		Body.render(matrixStack, buffer, packedLight, packedOverlay);
		Armleft.render(matrixStack, buffer, packedLight, packedOverlay);
		Armright.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, e);
	}
}